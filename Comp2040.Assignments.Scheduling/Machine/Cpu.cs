﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comp2040.Assignments.Scheduling.Algorithms;
using Comp2040.Assignments.Scheduling.Models;

namespace Comp2040.Assignments.Scheduling.Machine
{
    public class Cpu : ICpu
    {
        public const int TimeQuantum = 4;
        public const int ContextSwitchingTime = 0;
        public const int HighestPriority = 0;
        public const int LowestPriority = 5;

        public int CurrentTime { get; set; }

        //private List<SchedulingProcess> _readyProcesses = new List<SchedulingProcess> ();
        private MultiLevelProcessQueue _readyProcesses = new MultiLevelProcessQueue (LowestPriority + 1);
        private ISchedulingAlgorithm _schedulingAlgorithm;

        public readonly List<SchedulingProcess> FinishedProcesses = new List<SchedulingProcess> ();

        public bool HasReadyProcesses { get { return _readyProcesses.ContainsProcessesToRun (); }}

        public event EventHandler OnCpuCycle;

        private SchedulingProcess _currentProcess = null;

        public Cpu (ISchedulingAlgorithm schedulingAlgorithm)
        {
            _schedulingAlgorithm = schedulingAlgorithm;
            CurrentTime = 0;
        }

        public void AddArrivingProcess (IEnumerable<SchedulingProcess> processes)
        {
            if (processes.Count() < 1) return;

            Console.WriteLine ("Adding {0} processes to CPU at time {1}", processes.Count(), CurrentTime);
            AssignPriorityLevel (processes, HighestPriority);
            _readyProcesses.GetProcessQueue(HighestPriority).AddRange (processes);

            PreemptSchedulerIfRequired ();
        }

        private void AssignPriorityLevel(IEnumerable<SchedulingProcess> processes, int level)
        {
            foreach (var proc in processes) {
                proc.PriorityLevel = level;
                proc.SleepProcess(CurrentTime);
            }
        }

        private void PreemptSchedulerIfRequired()
        {
            if (_schedulingAlgorithm is IPreemptiveAlgorithm && _currentProcess != null) {
                Console.WriteLine ("Attempting to preempt process at time {0}", CurrentTime);
                ((IPreemptiveAlgorithm)_schedulingAlgorithm).Preempt (_currentProcess, this, _readyProcesses);
            }
        }

        public void Cycle ()
        {
            // Load active processes
            // Determine next process to run

            Console.WriteLine ("=== CPU starting to cycle at time {0} ===", CurrentTime);

            var nextProcessToRun = DetermineNextProcessToRun ();
            int timeBeforeRunningProcess = CurrentTime;

            RunProcess (nextProcessToRun);

            // DEBUG: Enforce cpu time to increment
            EnforceCpuTimeToIncrement (timeBeforeRunningProcess);
        }

        private void EnforceCpuTimeToIncrement(int lastKnownCpuTime)
        {
            if (lastKnownCpuTime == CurrentTime) {
                Console.WriteLine ("Cpu time not incremented. Forcing update");
                CurrentTime++;
            }
        }

        private void RunProcess(SchedulingProcess processToRun)
        {
            // If no processes to run. Empty Cycle.
            if (processToRun == null) {
                IncrementTime ();
            } else {
                _currentProcess = processToRun;
                _schedulingAlgorithm.Run (processToRun, this);
                RemoveProcessIfFinished (processToRun);
            }
        }

        private void IncrementTime()
        {
            Console.WriteLine ("No processes in CPU ready for processing. Incrementing time...");
            CurrentTime++;
        }

        private void RemoveProcessIfFinished(SchedulingProcess processToRemove)
        {
            // Check for finished process
            if (processToRemove.IsFinished) {
                RemoveCurrentProcess (processToRemove);
            }
        }

        private void RemoveCurrentProcess(SchedulingProcess processToRemove)
        {
            _readyProcesses.RemoveProcess (processToRemove);
            FinishedProcesses.Add (processToRemove);
            Console.WriteLine ("Removing completed process '{0}' from CPU at time {1}",
                               processToRemove.Name,
                               CurrentTime);
            _currentProcess = null;
        }

        public void PerformProcessing(SchedulingProcess proc, int cyclesToProcess)
        {
            for (int i = 0; i < cyclesToProcess; i++) {
                Console.WriteLine ("   Processing '{0}'...", proc.Name);
                CurrentTime++;

                if (OnCpuCycle != null)
                    OnCpuCycle (this, null);
            }
        }

        private SchedulingProcess DetermineNextProcessToRun ()
        {
            // Depends on Scheduling Algorithm.
            return _schedulingAlgorithm.NextProcessToRun (_readyProcesses);
        }

        public void LowerProcessPriority(SchedulingProcess proc)
        {
            // find process in multi level queue
            // downgrade if there
            // error if not there
            int currentLevel = _readyProcesses.GetPriorityForProcess (proc);
            int nextLevel = Math.Min (currentLevel + 1, Cpu.LowestPriority);

            if (currentLevel != LowestPriority) {

                _readyProcesses.MoveProcessToPriorityLevel (proc, nextLevel);
                proc.PriorityLevel = nextLevel;

                Console.WriteLine ("Process '{0}' has been moved from priority level {1} to {2}",
                                  proc.Name, currentLevel, nextLevel);
            } else {
                Console.WriteLine ("Process '{0}' is at lowest priority on level {1}",
                                  proc.Name, Cpu.LowestPriority);
            }


        }

    }
}
