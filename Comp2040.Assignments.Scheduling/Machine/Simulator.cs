﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comp2040.Assignments.Scheduling.Algorithms;
using Comp2040.Assignments.Scheduling.Models;

namespace Comp2040.Assignments.Scheduling.Machine
{
    public class Simulator
    {
        private List<SchedulingProcess> _processesToSimulate = new List<SchedulingProcess> ();
        private Cpu _cpu;

        public Simulator(List<SchedulingProcess> processes, ISchedulingAlgorithm algorithm)
        {
            _processesToSimulate = processes;
            _cpu = new Cpu (algorithm);
            _cpu.OnCpuCycle += OnCpuCycle;
        }

        public void Simulate ()
        {
            while (_processesToSimulate.Any () || _cpu.HasReadyProcesses) {

                FeedCpuAnyArrivingProcesses ();

                // Cycle CPU
                _cpu.Cycle ();
            }
        }

        private void FeedCpuAnyArrivingProcesses()
        {
            // Check for arriving processes
            var arrivingProcesses = GetArrivingProcesses ();

            // Feed arriving processes to CPU
            _cpu.AddArrivingProcess (arrivingProcesses);
        }

        private IEnumerable<SchedulingProcess> GetArrivingProcesses ()
        {
            // Find processes where time arrived is before now
            var processes = _processesToSimulate.Where (p => p.ArrivalTime <= _cpu.CurrentTime).ToList();

            // Remove processes from list
            processes.ForEach (p => _processesToSimulate.Remove (p));

            // Return processes
            return processes;
        }

        public void DisplayMetrics()
        {
            var procs = _cpu.FinishedProcesses.ToList ();

            Console.WriteLine ("Metrics:");
            Console.WriteLine ("{0,10} {1,15} {2,15} {3,20}", "Process", "Entry Time", "Waiting Time", "Turnaround Time");
            foreach (var proc in procs.OrderBy(p => p.ProcessId)) {

                // Display metrics
                Console.WriteLine ("{0,10} {1,15} {2,15} {3,20}",
                               proc.Name, proc.StartProcessingTime,
                               proc.TotalTimeWaitingAndSleeping,
                               proc.TotalTurnaroundTimeForCompletion);
            }

            int totalWaitingTime = procs.Sum (p => p.TotalTimeWaitingAndSleeping);
            int totalTurnaroundTime = procs.Sum (p => p.TotalTurnaroundTimeForCompletion);
            int procCount = procs.Count;

            Console.WriteLine ();
            Console.WriteLine ("{0,25} {1,25}", "Average Waiting Time", "Average Turnaround Time");
            Console.WriteLine ("{0,25} {1,25}", totalWaitingTime / (float)procCount, totalTurnaroundTime / (float)procCount);
        }

        private void OnCpuCycle (object sender, EventArgs e)
        {
            FeedCpuAnyArrivingProcesses ();
        }
    }
}
