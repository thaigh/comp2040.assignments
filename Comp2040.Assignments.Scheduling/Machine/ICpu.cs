﻿using System;
namespace Comp2040.Assignments.Scheduling.Machine
{
    public interface ICpu
    {
        void Cycle ();
    }
}
