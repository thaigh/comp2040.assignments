﻿using System;
namespace Comp2040.Assignments.Scheduling.Models
{
    public class SchedulingProcess
    {
        private static int NextProcessId = 1;

        public string Name { get; set; }
        public int ProcessId { get; private set; }
        public int TotalCpuCycles { get; set; }
        public int ArrivalTime { get; set; }
        public int PriorityLevel { get; set; }

        public int UsedCpuCycles { get; set; }
        public int RemainingCpuCycles { get { return TotalCpuCycles - UsedCpuCycles; } }

        public int StartProcessingTime { get; private set; }
        public int EndProcessingTime { get; private set; }
        public int LastWakeUpTime { get; private set; }
        public int LastSleepTime { get; private set; }

        public int TotalTimeSlept { get; private set; }

        public bool IsStarted { get; private set; }
        public bool IsFinished { get; private set;}

        public int TimeSpentWaitingUntilStart { get { return StartProcessingTime - ArrivalTime; } }
        public int TotalTimeWaitingAndSleeping { get { return TimeSpentWaitingUntilStart + TotalTimeSlept; } }
        public int TotalTurnaroundTimeForCompletion { get { return EndProcessingTime - ArrivalTime; } }

        public bool HasCompletedProcessingAllCycles { get { return RemainingCpuCycles <= 0; } }

        public SchedulingProcess()
        {
            ProcessId = NextProcessId;
            NextProcessId++;
        }

        public void StartProcess (int currentTime)
        {
            StartProcessingTime = currentTime;
            UsedCpuCycles = 0;
            SetStartFinish (false);

            Console.WriteLine ("Process '{0}' has started on CPU at time {1}", Name, currentTime);
        }

        public void FinishProcess (int currentTime)
        {
            EndProcessingTime = currentTime;
            UsedCpuCycles = TotalCpuCycles;
            SetStartFinish (true);

            Console.WriteLine ("Process '{0}' has finished on CPU at time {1}", Name, currentTime);
        }

        private void SetStartFinish(bool processIsFinished)
        {
            IsFinished = processIsFinished;
            IsStarted = !processIsFinished;
        }

        public void WakeUpProcess (int currentTime)
        {
            LastWakeUpTime = currentTime;

            // Calculate sleep duration
            int sleepDuration = currentTime - LastSleepTime;

            // Update total Sleep Time
            TotalTimeSlept += sleepDuration;

            Console.WriteLine ("Process '{0}' has woken up at time {1}", Name, currentTime);
        }

        public void PerformProcessing(int cyclesBeingProcessed)
        {
            UsedCpuCycles += cyclesBeingProcessed;
        }

        public void SleepProcess (int currentTime)
        {
            LastSleepTime = currentTime;

            //// Determine how many cycles the process was awake for
            //int totalAwakeTime = currentTime - LastWakeUpTime;

            //// Subtract from cycles spent on CPU
            //UsedCpuCycles += totalAwakeTime;

            Console.WriteLine ("Process '{0}' has been put to sleep at time {1}", Name, currentTime);
        }

        public void StartOrWakeUp (int currentCpuTime)
        {
            if (IsStarted)
                WakeUpProcess (currentCpuTime);
            else
                StartProcess (currentCpuTime);
        }

        public void SleepOrFinish (int currentCpuTime)
        {
            if (HasCompletedProcessingAllCycles)
                FinishProcess (currentCpuTime);
            else
                SleepProcess (currentCpuTime);
        }

    }
}
