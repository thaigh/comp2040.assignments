﻿using System.Collections.Generic;
using Comp2040.Assignments.Scheduling.Machine;
using Comp2040.Assignments.Scheduling.Models;

namespace Comp2040.Assignments.Scheduling.Algorithms
{
    public interface ISchedulingAlgorithm
    {
        void Run (SchedulingProcess proc, Cpu cpu);
        SchedulingProcess NextProcessToRun (MultiLevelProcessQueue procs);
    }
}