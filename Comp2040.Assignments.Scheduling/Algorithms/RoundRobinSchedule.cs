﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comp2040.Assignments.Scheduling.Machine;
using Comp2040.Assignments.Scheduling.Models;

namespace Comp2040.Assignments.Scheduling.Algorithms
{
    public class RoundRobinSchedule : ISchedulingAlgorithm
    {
        public SchedulingProcess NextProcessToRun (MultiLevelProcessQueue procs)
        {
            //var earliestProcs = procs
            //    .Where (p => p.LastSleepTime == procs.Min (p2 => p2.LastSleepTime));
            //var proc = earliestProcs
            //    .Where (p => p.ProcessId == earliestProcs.Min (p2 => p2.ProcessId));

            var proc = procs
                    .GetProcessQueue(Cpu.HighestPriority)
                    .OrderBy (p => p.LastSleepTime)
                    .ThenBy (p => p.ProcessId)
                    .FirstOrDefault ();
            return proc;
        }

        public void Run (SchedulingProcess proc, Cpu cpu)
        {
            // Wake process up
            // Determine processing time
            // Process for available time quantum
            // Sleep Process

            proc.StartOrWakeUp (cpu.CurrentTime);

            int timeAllowedToProcess = TimeAllowedToProcess (proc, Cpu.TimeQuantum);

            cpu.PerformProcessing(proc, timeAllowedToProcess);
            proc.PerformProcessing(timeAllowedToProcess);

            proc.SleepOrFinish (cpu.CurrentTime);
        }

        private int TimeAllowedToProcess(SchedulingProcess proc, int maxTimeAllowed)
        {
            int timeRemainingToProcess = proc.RemainingCpuCycles;
            return Math.Min (maxTimeAllowed, timeRemainingToProcess);
        }
    }
}
