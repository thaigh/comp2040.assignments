﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comp2040.Assignments.Scheduling.Machine;
using Comp2040.Assignments.Scheduling.Models;

namespace Comp2040.Assignments.Scheduling.Algorithms
{
    public class ShortestJobNextSchedule : ISchedulingAlgorithm
    {
        public SchedulingProcess NextProcessToRun (MultiLevelProcessQueue procs)
        {
            var proc = procs.GetProcessQueue(Cpu.HighestPriority)
                    .OrderBy (p => p.RemainingCpuCycles)
                    .ThenBy (p => p.ProcessId)
                    .FirstOrDefault ();
            return proc;
        }

        public void Run (SchedulingProcess proc, Cpu cpu)
        {
            // Sounds like it is just FCFS

            // Run process to completion.
            // Cue up CPU time
            proc.StartProcess (cpu.CurrentTime);
            cpu.PerformProcessing (proc, proc.RemainingCpuCycles);
            proc.FinishProcess (cpu.CurrentTime);
        }
    }
}
