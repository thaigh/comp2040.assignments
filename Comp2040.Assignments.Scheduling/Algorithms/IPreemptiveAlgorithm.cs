﻿using System;
using System.Collections.Generic;
using Comp2040.Assignments.Scheduling.Machine;
using Comp2040.Assignments.Scheduling.Models;

namespace Comp2040.Assignments.Scheduling.Algorithms
{
    public interface IPreemptiveAlgorithm
    {
        void Preempt (SchedulingProcess currentProcess, Cpu cpu, MultiLevelProcessQueue availableProcesses);
        bool HasBeenPreempted ();
    }
}
