﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comp2040.Assignments.Scheduling.Machine;
using Comp2040.Assignments.Scheduling.Models;

namespace Comp2040.Assignments.Scheduling.Algorithms
{
    public class FirstComeFirstServeSchedule : ISchedulingAlgorithm
    {
        public SchedulingProcess NextProcessToRun (MultiLevelProcessQueue procs)
        {
            var proc = procs
                .GetProcessQueue(Cpu.HighestPriority)
                .OrderBy (p => p.ArrivalTime)
                .ThenBy (p => p.ProcessId)
                .FirstOrDefault ();
            return proc;
        }

        public void Run (SchedulingProcess proc, Cpu cpu)
        {
            // Run process to completion.
            // Do not preempt when processes arrive

            // Run process to completion.
            // Cue up CPU time
            proc.StartProcess (cpu.CurrentTime);
            cpu.PerformProcessing (proc, proc.RemainingCpuCycles);
            proc.FinishProcess (cpu.CurrentTime);
        }
    }
}
