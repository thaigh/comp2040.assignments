﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comp2040.Assignments.Scheduling.Machine;
using Comp2040.Assignments.Scheduling.Models;

namespace Comp2040.Assignments.Scheduling.Algorithms
{
    public class ShortestRemainingTimeSchedule : ISchedulingAlgorithm, IPreemptiveAlgorithm
    {

        private bool _hasBeenPreempted = false;

        public bool HasBeenPreempted ()
        {
            return _hasBeenPreempted;
        }

        public SchedulingProcess NextProcessToRun (MultiLevelProcessQueue procs)
        {
            var proc = procs.GetProcessQueue (Cpu.HighestPriority)
                    .OrderBy (p => p.RemainingCpuCycles)
                    .ThenBy (p => p.ProcessId)
                    .FirstOrDefault ();
            return proc;
        }

        public void Preempt (SchedulingProcess currentProcess, Cpu cpu, MultiLevelProcessQueue availableProcesses)
        {
            // Check and see if there is a new process that should be running
            // Prempt to stop running current process
            if (currentProcess == null) return;

            var nextProc = NextProcessToRun (availableProcesses);
            if (nextProc.ProcessId != currentProcess.ProcessId)
            {
                _hasBeenPreempted = true;
                Console.WriteLine ("Process '{0}' has been preempted in favour of '{1}'",
                                  currentProcess.Name, nextProc.Name);
            }

            // May have to clean up currentProcess if we are preempted
                
        }

        public void Run (SchedulingProcess proc, Cpu cpu)
        {
            // Normally run to completion, but with possibility of being
            // preempted during processing

            _hasBeenPreempted = false;

            proc.StartOrWakeUp (cpu.CurrentTime);

            int timeRequiredToProcess = proc.RemainingCpuCycles;

            while (!proc.HasCompletedProcessingAllCycles && !HasBeenPreempted())
            {
                proc.PerformProcessing (1); // Has to come before due to preemting
                cpu.PerformProcessing (proc, 1);
            }

            // We have broken out of while loop.
            // Either we have finished processing, or we were
            // preempted.
            proc.SleepOrFinish (cpu.CurrentTime);

        }

    }
}
