﻿using System;
using Comp2040.Assignments.Scheduling.Machine;
using Comp2040.Assignments.Scheduling.Models;
using System.Linq;

namespace Comp2040.Assignments.Scheduling.Algorithms
{
    public class MultiLevelFeedbackQueueSchedule : ISchedulingAlgorithm
    {

        public SchedulingProcess NextProcessToRun (MultiLevelProcessQueue procs)
        {
            // Look in each queue from highest to lowest
            // find first process at head of queue (Time waiting / Arrival / ProcessId)

            if (!procs.ContainsProcessesToRun ()) return null;

            //for (int i = Cpu.HighestPriority; i <= Cpu.LowestPriority; i++)
            //{
            //    // get processes in queue
            //    if (procs.QueueLevelContainsProcesses(i))
            //    {
            //        var proc = procs.GetProcessQueue (i)
            //                        .OrderBy (p => p.LastWakeUpTime)
            //                        .ThenBy (p => p.ProcessId)
            //                        .FirstOrDefault();
            //        return proc;
            //    }
            //}

            //// nothing found
            //return null;

            var proc = procs.AllProcesses()
                            .OrderBy(p => p.PriorityLevel)
                            .ThenBy (p => p.LastSleepTime)
                            .ThenBy (p => p.ProcessId)
                            .FirstOrDefault();
            return proc;

        }

        public void Run (SchedulingProcess proc, Cpu cpu)
        {
            // Round robin using time quantum

            // Wake process up
            // Determine processing time
            // Process for available time quantum
            // Sleep Process

            proc.StartOrWakeUp (cpu.CurrentTime);

            int timeAllowedToProcess = TimeAllowedToProcess (proc, Cpu.TimeQuantum);

            cpu.PerformProcessing (proc, timeAllowedToProcess);
            proc.PerformProcessing (timeAllowedToProcess);

            proc.SleepOrFinish (cpu.CurrentTime);

            // If we did not finish processing, move to next level of queue
            if (!proc.IsFinished)
                cpu.LowerProcessPriority (proc);
        }

        private int TimeAllowedToProcess (SchedulingProcess proc, int maxTimeAllowed)
        {
            int timeRemainingToProcess = proc.RemainingCpuCycles;
            return Math.Min (maxTimeAllowed, timeRemainingToProcess);
        }

    }
}
