﻿using System;
using System.Collections.Generic;
using Comp2040.Assignments.Scheduling.Algorithms;
using Comp2040.Assignments.Scheduling.Machine;
using Comp2040.Assignments.Scheduling.Models;

namespace Comp2040.Assignments.Scheduling
{
    class MainClass
    {
        public static void Main (string [] args)
        {
            List<SchedulingProcess> set1 = new List<SchedulingProcess>
            {
                new SchedulingProcess { Name = "p1", TotalCpuCycles = 10, ArrivalTime = 0 },
                new SchedulingProcess { Name = "p2", TotalCpuCycles = 1, ArrivalTime = 0 },
                new SchedulingProcess { Name = "p3", TotalCpuCycles = 2, ArrivalTime = 0 },
                new SchedulingProcess { Name = "p4", TotalCpuCycles = 1, ArrivalTime = 0 },
                new SchedulingProcess { Name = "p5", TotalCpuCycles = 5, ArrivalTime = 0 },
            };

            List<SchedulingProcess> set2 = new List<SchedulingProcess>
            {
                new SchedulingProcess { Name = "p1", TotalCpuCycles = 10, ArrivalTime = 0 },
                new SchedulingProcess { Name = "p2", TotalCpuCycles = 1, ArrivalTime = 2 },
                new SchedulingProcess { Name = "p3", TotalCpuCycles = 2, ArrivalTime = 6 },
                new SchedulingProcess { Name = "p4", TotalCpuCycles = 1, ArrivalTime = 10 },
                new SchedulingProcess { Name = "p5", TotalCpuCycles = 5, ArrivalTime = 14 },
            };

            //Simulator sim = new Simulator (set2, new FirstComeFirstServeSchedule ());
            //Simulator sim = new Simulator (set2, new RoundRobinSchedule ());
            //Simulator sim = new Simulator (set2, new ShortestRemainingTimeSchedule ());
            Simulator sim = new Simulator (set2, new ShortestJobNextSchedule ());
            sim.Simulate ();
            sim.DisplayMetrics ();
        }
    }
}
