﻿using System;
using Comp2040.Assignments.Memory.Machine;
using Comp2040.Assignments.Memory.Models;
using System.Security.Cryptography.X509Certificates;

namespace Comp2040.Assignments.Memory.Algorithms.Replacement
{
    public interface IPageReplacementAlgorithm
    {

        /// <summary>
        /// Gets the replacement frame index using a custom defined method of the underlying Page Replacement Algoritm
        /// </summary>
        /// <returns>The replacement frame index in the Page Memory Block</returns>
        /// <param name="memory">The Page Frame Memory Block to replace the frame in</param>
        int GetReplacementFrame(SchedulingProcess proc, PageMemoryBlock memory);



        /// <summary>
        /// Gets the replacement frame index using local replacement strategy
        /// 
        /// Performs a localised search for all pages beloning to the given SchedulingProcess
        /// and either finds an empty frame index in memory to load the Page,
        /// or a frame index occupied by a page from the given SchedulingProcess
        /// </summary>
        /// <returns>The replacement frame index in the Page Memory Block</returns>
        /// <param name="proc">The Scheduling Process in which to replace a page local to this process</param>
        /// <param name="memory">The Page Frame Memory Block to replace the frame in</param>
        int GetReplacementFrameUsingLocalReplacement(SchedulingProcess proc, PageMemoryBlock memory);

        /// <summary>
        /// Gets the replacement frame index using global replacement strategy.
        /// 
        /// Performs a global search through all Page Frames in the PageMemoryBlock and either
        /// finds an empty fram index in memory to load the page, or finds a frame index occupied
        /// by any process that can be replaced.
        /// </summary>
        /// <returns>The replacement frame index in the Page Memory Block</returns>
        /// <param name="memory">The Page Frame Memory Block to replace the frame in</param>
        int GetReplacementFrameUsingGlobalReplacement(SchedulingProcess proc, PageMemoryBlock memory);
    }
}
