﻿using System;
using Comp2040.Assignments.Memory.Machine;
using Comp2040.Assignments.Memory.Models;
using System.Collections.Generic;
using System.Linq;

namespace Comp2040.Assignments.Memory.Algorithms.Replacement
{
    public class LeastRecentlyUsedReplacementAlgorithm : IPageReplacementAlgorithm
    {
        public ReplacementStrategy Strategy { get; private set; }

        public LeastRecentlyUsedReplacementAlgorithm(ReplacementStrategy strategy)
        {
            Strategy = strategy;
        }

        public int GetReplacementFrame(SchedulingProcess proc, PageMemoryBlock memory)
        {
            switch (Strategy)
            {
                case ReplacementStrategy.Local: return GetReplacementFrameUsingLocalReplacement(proc, memory);
                case ReplacementStrategy.Global: return GetReplacementFrameUsingGlobalReplacement(proc, memory);
                default: throw new Exception("Replacement strategy not defined for " + Strategy);
            }
        }


        #region LocalReplacement

        public int GetReplacementFrameUsingLocalReplacement(SchedulingProcess proc, PageMemoryBlock memory)
        {
            // Looks at all pages for the given process and replaces the least recently used frame
            // Does not affect any other process's frames

            switch (memory.AllocationStrategy)
            {
                case AllocationStrategy.FixedFrame: return LocalisedFixedFrameReplacement(proc, memory);
                case AllocationStrategy.Variable: return LocalisedVariableReplacement(proc, memory);
                default: throw new Exception("Page Memory Allocation Strategy is undefined for " + memory.AllocationStrategy);
            }
        }

        private int LocalisedFixedFrameReplacement(SchedulingProcess proc, PageMemoryBlock memory)
        {
            // The process is restricted in how many frames is is allowed to allocate.
            // Check to see if we have allocated the max number of frames for this process.
            // If we havent allocated the max number of frames, we can simply find an empty space in memory.
            // If there is no free space in memory, or if we have allocated the max number of frames,
            // we need to replace the least recently used frame for this process

            IEnumerable<PageFrameMap> frames = memory.GetPagesForProcess(proc);

            // Find free space if possible (as per defined above)
            if (frames.Count() < memory.MaxFramesPerProcess && !memory.IsFull)
                return memory.NextEmptyFrame();

            // Either we have allocated the Max Number of Frames for this process (which means we are guaranteed
            // to find a Frame to replace), or the memory is full (in which case we may not have a possible
            // frame to replace, since this is a localised search to only this process, and this process may not
            // have ANY frames in memory)

            // Find least recently used frame for process
            // Exclude any frames that have not been used
            PageFrameMap leastRecentFrame = frames
                .Where(f => f.Page.HasBeenUsed)
                .OrderBy(f => f.Page.LastUsedTime)
                .FirstOrDefault();

            // Frame is either null (we didn't find one), or is set (we have a frame to replace)
            return (leastRecentFrame != null) ? leastRecentFrame.FrameIndex : -1;

            // We could check again to see if we have a frame that IS in memory, but hasn't been used, but this
            // is kind of pointless (And I think this case should also be excluded, since you woudn't be loading
            // in a frame for a process if previous frames haven't been used - unless you were loading several
            // frames in advance, in which you don't want to remove the first frame anyways because you're going to
            // need that well before you need this frame)
        }

        private int LocalisedVariableReplacement(SchedulingProcess proc, PageMemoryBlock memory)
        {
            // The process is allowed to allocate as many frames as it requires.
            // Find any empty space available in memory for the frame,
            // Or if there is no space available, then replace the least recently used frame for this process.

            if (!memory.IsFull)
                return memory.NextEmptyFrame();

            // The memory is full. Find the least recently used frame for the process
            // Excluding frames that have yet to be used
            IEnumerable<PageFrameMap> frames = memory.GetPagesForProcess(proc);
            PageFrameMap leastRecentFrame = frames
                .Where(f => f.Page.HasBeenUsed)
                .OrderBy(f => f.Page.LastUsedTime)
                .FirstOrDefault();

            // Frame is either null (we didn't find one), or is set (we have a frame to replace)
            return (leastRecentFrame != null) ? leastRecentFrame.FrameIndex : -1;
        }

        #endregion


        #region GlobalReplacement

        public int GetReplacementFrameUsingGlobalReplacement(SchedulingProcess proc, PageMemoryBlock memory)
        {
            // Looks at all pages for all processes.
            // Is allowed to replace the least recently used frame from ANY process

            switch (memory.AllocationStrategy)
            {
                case AllocationStrategy.FixedFrame: return GlobalFixedFrameReplacement(proc, memory);
                case AllocationStrategy.Variable: return GlobalVariableReplacement(proc, memory);
                default: throw new Exception("Page Memory Allocation Strategy is undefined for " + memory.AllocationStrategy);
            }
        }

        private int GlobalFixedFrameReplacement(SchedulingProcess proc, PageMemoryBlock memory)
        {
            // Each process is allocated a fixed number of frames to store in Page Memory
            // If the process has not allocated its maximum number of frames, we can find ANY frame
            // in memory to replace (regardless of whether Paged Memory is full or not).
            // If this process has already reached its limit, then we have to perform a localised
            // search and replace the least recently used page for this process.

            IEnumerable<PageFrameMap> frames = memory.GetPagesForProcess(proc);
            if (frames.Count() < memory.MaxFramesPerProcess)
            {
                // Find any empty frame in memory, or the least recently used frame
                // out of all processes
                return (!memory.IsFull) ? memory.NextEmptyFrame() : memory.LeastRecentlyUsedPage().FrameIndex;
            }

            // The process has already allocated the maximum number of frames allowed.
            // Perform a localised search

            // Find least recently used frame for process
            // Exclude any frames that have not been used
            PageFrameMap leastRecentFrame = frames
                .Where(f => f.Page.HasBeenUsed)
                .OrderBy(f => f.Page.LastUsedTime)
                .FirstOrDefault();

            // Frame is either null (we didn't find one), or is set (we have a frame to replace)
            return (leastRecentFrame != null) ? leastRecentFrame.FrameIndex : -1;
        }

        private int GlobalVariableReplacement(SchedulingProcess proc, PageMemoryBlock memory)
        {
            // The process is allowed to allocate as many frames as it requires.
            // Either find the next empty space in memory, or replace the least recently used frame
            // out of all processes in memory

            return (!memory.IsFull) ? memory.NextEmptyFrame() : memory.LeastRecentlyUsedPage().FrameIndex;

        }

        #endregion


    }
}
