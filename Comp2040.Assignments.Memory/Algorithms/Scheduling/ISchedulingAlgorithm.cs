﻿using System.Collections.Generic;
using Comp2040.Assignments.Memory.Machine;
using Comp2040.Assignments.Memory.Models;

namespace Comp2040.Assignments.Memory.Algorithms.Scheduling
{
    public interface ISchedulingAlgorithm
    {
        void Run (SchedulingProcess proc, Cpu cpu);
        SchedulingProcess NextProcessToRun (MultiLevelProcessQueue procs);
    }
}