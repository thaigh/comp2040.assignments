﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comp2040.Assignments.Memory.Machine;
using Comp2040.Assignments.Memory.Models;

namespace Comp2040.Assignments.Memory.Algorithms.Scheduling
{
    public class RoundRobinSchedule : ISchedulingAlgorithm
    {
        public const int TimeQuantum = 4;

        public SchedulingProcess NextProcessToRun (MultiLevelProcessQueue procs)
        {
            //var earliestProcs = procs
            //    .Where (p => p.LastSleepTime == procs.Min (p2 => p2.LastSleepTime));
            //var proc = earliestProcs
            //    .Where (p => p.ProcessId == earliestProcs.Min (p2 => p2.ProcessId));

            var proc = procs
                    .GetProcessQueue(Cpu.HighestPriority)
                    .OrderBy (p => p.LastSleepTime)
                    .ThenBy (p => p.ProcessId)
                    .FirstOrDefault ();
            return proc;
        }

        public void Run (SchedulingProcess proc, Cpu cpu)
        {
            // Wake process up
            // Determine processing time
            // Process for available time quantum
            // Sleep Process

            proc.StartOrWakeUp (cpu.CurrentTime);

            int timeAllowedToProcess = TimeAllowedToProcess (proc, TimeQuantum);

            for (int i = 0; i < timeAllowedToProcess && !proc.IsFinished; i++)
            {
                cpu.PerformProcessing (proc, 1);
                //proc.PerformProcessing (1);
            }

            proc.SleepOrFinish (cpu.CurrentTime);
        }

        private int TimeAllowedToProcess(SchedulingProcess proc, int maxTimeAllowed)
        {
            int timeRemainingToProcess = proc.RemainingCpuCycles;
            return Math.Min (maxTimeAllowed, timeRemainingToProcess);
        }
    }
}
