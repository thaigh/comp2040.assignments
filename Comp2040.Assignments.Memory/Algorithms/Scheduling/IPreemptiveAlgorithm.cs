﻿using System;
using System.Collections.Generic;
using Comp2040.Assignments.Memory.Machine;
using Comp2040.Assignments.Memory.Models;

namespace Comp2040.Assignments.Memory.Algorithms.Scheduling
{
    public interface IPreemptiveAlgorithm
    {
        void Preempt (SchedulingProcess currentProcess, Cpu cpu, MultiLevelProcessQueue availableProcesses);
        bool HasBeenPreempted ();
    }
}
