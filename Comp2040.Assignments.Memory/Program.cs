﻿using System;
using System.Collections.Generic;
using Comp2040.Assignments.Memory.Models;
using Comp2040.Assignments.Memory.Machine;
using Comp2040.Assignments.Memory.Algorithms.Scheduling;
using Comp2040.Assignments.Memory.Algorithms.Replacement;

namespace Comp2040.Assignments.Memory
{
    class MainClass
    {
        public static void Main (string [] args)
        {

            PageGeneratorFactory gen = new PageGeneratorFactory ();

            List<SchedulingProcess> processes = new List<SchedulingProcess> {
                new SchedulingProcess(
                    new List<Page> {
                        gen.GeneratePage(1,1), gen.GeneratePage(1,2), gen.GeneratePage(1,3),
                        gen.GeneratePage(1,1), gen.GeneratePage(1,2), gen.GeneratePage(1,3)
                    }
                ) { Name = "P1" },
                //new SchedulingProcess(
                //    new List<Page> {
                //        gen.GeneratePage(2,1), gen.GeneratePage(2,2), gen.GeneratePage(2,3),
                //        gen.GeneratePage(2,4), gen.GeneratePage(2,5), gen.GeneratePage(2,6)
                //    }
                //) { Name = "P2" },
                //new SchedulingProcess(
                //    new List<Page> {
                //        gen.GeneratePage(3,1), gen.GeneratePage(3,1), gen.GeneratePage(3,1),
                //        gen.GeneratePage(3,4), gen.GeneratePage(3,5), gen.GeneratePage(3,6)
                //    }
                //) { Name = "P3" }
            };

            Simulator sim = new Simulator(processes, new RoundRobinSchedule(), new LeastRecentlyUsedReplacementAlgorithm(ReplacementStrategy.Local));
            sim.Simulate();


        }
    }
}
