﻿using System;
using System.Collections.Generic;
namespace System.Linq
{
    public static class LinqExtensions
    {
        public static bool Contains<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            var first = source.FirstOrDefault(predicate);
            return IsDefault(first);
        }

        public static object GetDefault(this Type t)
        {
            return t.IsValueType ? Activator.CreateInstance(t) : null;
        }

        private static T GetDefault<T>()
        {
            var t = typeof(T);
            return (T)GetDefault(t);
        }

        private static bool IsDefault<T>(T other)
        {
            T defaultValue = GetDefault<T>();
            if (other == null) return defaultValue == null;
            return other.Equals(defaultValue);
        }
    }
}
