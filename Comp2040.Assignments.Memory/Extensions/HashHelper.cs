﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    public static class HashHelper
    {
        private static int InitialHash = 17; // Prime number
        private static int Multiplier = 23; // Different prime number

        /// <summary>
        /// http://stackoverflow.com/a/6424804
        /// http://stackoverflow.com/a/263416
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public static Int32 GetHashCode (params object [] values)
        {
            unchecked // overflow is fine
            {
                int hash = InitialHash;

                if (values != null)
                    for (int i = 0; i < values.Length; i++) {
                        object currentValue = values [i];
                        hash = hash * Multiplier
                            + (currentValue != null ? currentValue.GetHashCode () : 0);
                    }

                return hash;
            }
        }
    }
}