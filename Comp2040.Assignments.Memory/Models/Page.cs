﻿using System;
using System.Collections.Generic;

namespace Comp2040.Assignments.Memory.Models
{
    public class Page
    {

        // A page only has one instruction
        private static readonly Instruction Inst = new Instruction();
        public IEnumerable<Instruction> Instructions {
            get {
                return new List<Instruction> { Inst };
            }
        }

        private static int NextPageId = 0;
        private static int GetNextId() { return NextPageId++; }
        public int PageId { get; private set; }

        public int ProcessId { get { return ParentProcess.ProcessId; }}
        public int PageNumber { get; private set; }

        public bool LoadedInMemory { get; set; }

        public SchedulingProcess ParentProcess { get; private set; }
        public bool ParentProcessIsFinished { get { return ParentProcess.IsFinished; } }

        public int LastUsedTime { get; set; }
        public Boolean HasBeenUsed { get { return LastUsedTime > -1; }}

        public Page(int pageNumber)
        {
            this.PageNumber = pageNumber;
            PageId = GetNextId();

            LastUsedTime = -1;
        }

        public void LinkToProcess(SchedulingProcess parent)
        {
            this.ParentProcess = parent;
        }

        public override string ToString ()
        {
            return string.Format ("{0}:{1}", ParentProcess.Name, PageNumber);
        }

    }
}
