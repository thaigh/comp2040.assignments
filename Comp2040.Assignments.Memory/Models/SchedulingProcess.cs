﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Comp2040.Assignments.Memory.Models
{
    public class SchedulingProcess
    {
        private static int NextProcessId = 1;

        public string Name { get; set; }
        public int ProcessId { get; private set; }
        public int ArrivalTime { get; set; }
        public int PriorityLevel { get; set; }

        public int TotalCpuCycles { get { return _pages.Count; } }
        public int RemainingCpuCycles { get { return TotalCpuCycles - CurrentPageIndex; } }

        public int StartProcessingTime { get; private set; }
        public int EndProcessingTime { get; private set; }
        public int LastWakeUpTime { get; private set; }
        public int LastSleepTime { get; private set; }

        public int TotalTimeSlept { get; private set; }

        public int TimeSpentWaitingUntilStart { get { return StartProcessingTime - ArrivalTime; } }
        public int TotalTimeWaitingAndSleeping { get { return TimeSpentWaitingUntilStart + TotalTimeSlept; } }
        public int TotalTurnaroundTimeForCompletion { get { return EndProcessingTime - ArrivalTime; } }

        public bool HasCompletedProcessingAllCycles { get { return RemainingCpuCycles <= 0; } }

        private List<Page> _pages = new List<Page>();
        public int CurrentPageIndex { get; private set; }
        public int PageCount { get { return _pages.Count; } }

        public ProcessState State { get; private set; }

        public SchedulingProcess(IEnumerable<Page> processPages)
        {
            ProcessId = NextProcessId;
            NextProcessId++;

            AddPages(processPages);
            State = ProcessState.WaitingToStart;
        }


        #region StateTransition

        public void StartProcess(int currentTime)
        {
            StartProcessingTime = currentTime;
            CurrentPageIndex = 0;

            Console.WriteLine("Process '{0}' has started on CPU at time {1}", Name, currentTime);
            State = ProcessState.Running;
        }

        public void FinishProcess(int currentTime)
        {
            EndProcessingTime = currentTime;
            CurrentPageIndex = TotalCpuCycles;

            Console.WriteLine("Process '{0}' has finished on CPU at time {1}", Name, currentTime);
            State = ProcessState.Finished;
        }

        public void WakeUpProcess(int currentTime)
        {
            LastWakeUpTime = currentTime;

            // Calculate sleep duration
            int sleepDuration = currentTime - LastSleepTime;

            // Update total Sleep Time
            TotalTimeSlept += sleepDuration;

            Console.WriteLine("Process '{0}' has woken up at time {1}", Name, currentTime);
            State = ProcessState.Running;
        }

        public void PerformProcessing(int cyclesBeingProcessed)
        {
            CurrentPageIndex += cyclesBeingProcessed;
        }

        public void SleepProcess(int currentTime)
        {
            LastSleepTime = currentTime;

            Console.WriteLine("Process '{0}' has been put to sleep at time {1}", Name, currentTime);
            State = ProcessState.Sleeping;
        }

        public void StartOrWakeUp(int currentCpuTime)
        {
            if (IsStarted)
                WakeUpProcess(currentCpuTime);
            else
                StartProcess(currentCpuTime);
        }

        public void SleepOrFinish(int currentCpuTime)
        {
            if (HasCompletedProcessingAllCycles)
                FinishProcess(currentCpuTime);
            else
                SleepProcess(currentCpuTime);
        }

        public void Block() { State = ProcessState.Blocked; }
        public void Unblock() { State = ProcessState.Sleeping; }

        public bool HasNotStarted { get { return State == ProcessState.WaitingToStart; } }
        public bool IsStarted { get { return State != ProcessState.WaitingToStart; } }
        public bool IsRunning { get { return State == ProcessState.Running; } }
        public bool IsSleeping { get { return State == ProcessState.Sleeping; } }
        public bool IsBlocked { get { return State == ProcessState.Blocked; } }
        public bool IsFinished { get { return State == ProcessState.Finished; } }

        #endregion


        #region PageManagement

        private void AddPages(IEnumerable<Page> processPages)
        {
            foreach (Page p in processPages)
            {
                _pages.Add(p);
                p.LinkToProcess(this);
            }
        }

        public Page GetPageAtIndex(int pageIndex)
        {
            return _pages[pageIndex];
        }

        public Page CurrentPage { get { return _pages[CurrentPageIndex]; } }

        public bool ContainsPage(Page p)
        {
            if (p == null) return false;

            return this.ProcessId == p.ParentProcess.ProcessId
                       && _pages.Contains(page => page.PageId == p.PageId);
        }

        #endregion


    }
}
