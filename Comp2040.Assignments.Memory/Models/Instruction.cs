﻿using System;
namespace Comp2040.Assignments.Memory.Models
{
    public class Instruction
    {
        public const int ExecutionTime = 1;

        public int TimeToExecute { get { return ExecutionTime; } }
        public void Execute() { Console.WriteLine("        Executing Instruction..."); }
    }
}
