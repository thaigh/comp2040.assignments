﻿using System;
namespace Comp2040.Assignments.Memory.Models
{
    public enum ProcessState
    {
        WaitingToStart,
        Running,
        Blocked,
        Sleeping,
        Finished
    }
}
