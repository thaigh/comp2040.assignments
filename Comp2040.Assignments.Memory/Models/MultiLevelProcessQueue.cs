﻿using System;
using System.Collections.Generic;
using Comp2040.Assignments.Memory.Models;
using System.Linq;

namespace Comp2040.Assignments.Memory.Machine
{
    public class MultiLevelProcessQueue
    {
        private List<SchedulingProcess> [] _queues;
        private int _numberOfQueues;

        public MultiLevelProcessQueue (int numberOfQueues)
        {
            _queues = new List<SchedulingProcess> [numberOfQueues];
            _numberOfQueues = numberOfQueues;
            InitialiseQueues ();
        }

        private void InitialiseQueues()
        {
            for (int i = 0; i < _numberOfQueues; i++)
            {
                _queues [i] = new List<SchedulingProcess> ();
            }
        }

        public List<SchedulingProcess> GetProcessQueue(int level)
        {
            BoundsCheckPriorityLevel (level);

            return _queues [level];
        }

        public bool ContainsProcessesToRun()
        {
            foreach (var queue in _queues) {
                if (queue.Any ()) return true;
            }

            // none
            return false;
        }

        public IEnumerable<SchedulingProcess> AllProcesses()
        {
            foreach (var queue in _queues) {
                foreach (var proc in queue) {
                    if (proc != null) yield return proc;
                }
            }
        }

        public void RemoveProcess(SchedulingProcess proc)
        {
            foreach(var queue in _queues)
            {
                if (queue.Contains (proc)) {
                    queue.Remove (proc);
                    return;
                }
            }
        }

        public bool QueueLevelContainsProcesses(int level)
        {
            var queue = GetProcessQueue (level);
            return queue.Any ();
        }

        public int GetPriorityForProcess(SchedulingProcess proc)
        {
            for (int i = 0; i < _numberOfQueues; i++)
            {
                var queue = GetProcessQueue (i);
                if (queue.Contains(proc))
                    return i;
            }

            // not found
            throw new ArgumentException ("Process not found in queues");
        }

        public void MoveProcessToPriorityLevel(SchedulingProcess proc, int level)
        {
            BoundsCheckPriorityLevel (level);

            // find process
            int currentLevel = GetPriorityForProcess (proc);

            // remove process from current level
            _queues [currentLevel].Remove (proc);

            // add to new level
            _queues [level].Add (proc);
        }

        private void BoundsCheckPriorityLevel(int level)
        {
            if (level < 0 || level >= _numberOfQueues)
                throw new ArgumentOutOfRangeException (nameof (level), "Priority level out of bounds");
        }

    }
}
