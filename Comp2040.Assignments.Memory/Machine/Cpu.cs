﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comp2040.Assignments.Memory.Algorithms.Scheduling;
using Comp2040.Assignments.Memory.Models;
using Comp2040.Assignments.Memory.Algorithms.Replacement;

namespace Comp2040.Assignments.Memory.Machine
{
    public class PageFaultEventHandler
    {
        public Page PageToLoad { get; private set; }
        public PageFaultEventHandler(Page pageToLoad) { this.PageToLoad = pageToLoad; }
    }

    public class Cpu : ICpu
    {
        public const int TimeQuantum = 4;
        public const int HighestPriority = 0;
        public const int LowestPriority = 5;

        public int CurrentTime { get; set; }

        private MultiLevelProcessQueue _readyProcesses = new MultiLevelProcessQueue (LowestPriority + 1);
        private ISchedulingAlgorithm _schedulingAlgorithm;
        public readonly List<SchedulingProcess> FinishedProcesses = new List<SchedulingProcess> ();

        private IOController _ioController;
        private PageMemoryBlock _pageMemory;

        public bool HasReadyProcesses { get { return _readyProcesses.ContainsProcessesToRun (); }}

        public event EventHandler OnCpuCycle;
        public event EventHandler<PageFaultEventHandler> PageFault;

        private SchedulingProcess _currentProcess = null;

        public Cpu (ISchedulingAlgorithm schedulingAlgorithm, IPageReplacementAlgorithm replacementAlgorithm)
        {
            _schedulingAlgorithm = schedulingAlgorithm;
            CurrentTime = 0;

            _ioController = new IOController(this);
            _pageMemory = new PageMemoryBlock(_ioController, replacementAlgorithm);
            _pageMemory.UseFixedFrameAllocationStrategy(1);
        }

        public void AddArrivingProcess (IEnumerable<SchedulingProcess> processes)
        {
            if (processes.Count() < 1) return;

            Console.WriteLine ("Adding {0} processes to CPU at time {1}", processes.Count(), CurrentTime);
            AssignPriorityLevel (processes, HighestPriority);
            _readyProcesses.GetProcessQueue(HighestPriority).AddRange (processes);

            PreemptSchedulerIfRequired ();
        }

        private void AssignPriorityLevel(IEnumerable<SchedulingProcess> processes, int level)
        {
            foreach (var proc in processes) {
                proc.PriorityLevel = level;
                proc.SleepProcess(CurrentTime);
            }
        }

        private void PreemptSchedulerIfRequired()
        {
            if (_schedulingAlgorithm is IPreemptiveAlgorithm && _currentProcess != null) {
                Console.WriteLine ("Attempting to preempt process at time {0}", CurrentTime);
                ((IPreemptiveAlgorithm)_schedulingAlgorithm).Preempt (_currentProcess, this, _readyProcesses);
            }
        }

        public void Cycle ()
        {
            // Load active processes
            // Determine next process to run

            Console.WriteLine ("=== CPU starting to cycle at time {0} ===", CurrentTime);

            var nextProcessToRun = DetermineNextProcessToRun ();
            int timeBeforeRunningProcess = CurrentTime;

            RunProcess (nextProcessToRun);

            // DEBUG: Enforce cpu time to increment
            EnforceCpuTimeToIncrement (timeBeforeRunningProcess);
        }

        private void EnforceCpuTimeToIncrement(int lastKnownCpuTime)
        {
            if (lastKnownCpuTime == CurrentTime) {
                Console.WriteLine ("Cpu time not incremented. Forcing update");
                IncrementCpuTime(1);
            }
        }

        private void RunProcess(SchedulingProcess processToRun)
        {
            // If no processes to run. Empty Cycle.
            if (processToRun == null) {
                IncrementTime ();
            } else {
                _currentProcess = processToRun;
                _schedulingAlgorithm.Run (processToRun, this);
                RemoveProcessIfFinished (processToRun);
            }
        }

        private void IncrementTime()
        {
            Console.WriteLine ("No processes in CPU ready for processing. Incrementing time...");
            IncrementCpuTime(1);
        }

        private void RemoveProcessIfFinished(SchedulingProcess processToRemove)
        {
            // Check for finished process
            if (processToRemove.IsFinished) {
                RemoveCurrentProcess (processToRemove);
            }
        }

        private void RemoveCurrentProcess(SchedulingProcess processToRemove)
        {
            _readyProcesses.RemoveProcess (processToRemove);
            FinishedProcesses.Add (processToRemove);
            Console.WriteLine ("Removing completed process '{0}' from CPU at time {1}",
                               processToRemove.Name,
                               CurrentTime);
            _currentProcess = null;
        }

        public void PerformProcessing(SchedulingProcess proc, int cyclesToProcess)
        {
            for (int i = 0; i < cyclesToProcess; i++) {

                if (!PageIsInMemory(proc.CurrentPage))
                {
                    TriggerPageFault(proc);
                    IncrementCpuTime(1);
                }
                else
                {
                    Console.WriteLine("   Processing '{0}'...", proc.Name);
                    ExecutePageInstructions(proc);
                }
            }
        }

        private void ExecutePageInstructions(SchedulingProcess proc)
        {
            IncrementCpuTime(1);
            proc.PerformProcessing(1);
        }

        private void IncrementCpuTime(int cycles)
        {
            CurrentTime += cycles;
            OnCpuCycle?.Invoke(this, null);
        }

        private void TriggerPageFault(SchedulingProcess proc)
        {
            Console.WriteLine ("Page {0} for Process {1} is not in memory", proc.CurrentPage.PageNumber, proc.Name);
            proc.Block ();

            if (PageFault != null)
                PageFault (this, new PageFaultEventHandler (proc.CurrentPage));
        }

        private SchedulingProcess DetermineNextProcessToRun ()
        {
            // Depends on Scheduling Algorithm.
            return _schedulingAlgorithm.NextProcessToRun (_readyProcesses);
        }

        public void LowerProcessPriority(SchedulingProcess proc)
        {
            // find process in multi level queue
            // downgrade if there
            // error if not there
            int currentLevel = _readyProcesses.GetPriorityForProcess (proc);
            int nextLevel = Math.Min (currentLevel + 1, Cpu.LowestPriority);

            if (currentLevel != LowestPriority) {

                _readyProcesses.MoveProcessToPriorityLevel (proc, nextLevel);
                proc.PriorityLevel = nextLevel;

                Console.WriteLine ("Process '{0}' has been moved from priority level {1} to {2}",
                                  proc.Name, currentLevel, nextLevel);
            } else {
                Console.WriteLine ("Process '{0}' is at lowest priority on level {1}",
                                  proc.Name, Cpu.LowestPriority);
            }
        }

        public bool PageIsInMemory(SchedulingProcess proc, int pageIndex)
        {
            Page p = proc.GetPageAtIndex (pageIndex);
            return PageIsInMemory (p);
        }

        private bool PageIsInMemory(Page p)
        {
            return _pageMemory.ContainsPage (p);
        }

    }
}
