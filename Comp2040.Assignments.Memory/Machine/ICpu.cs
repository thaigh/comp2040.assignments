﻿using System;
namespace Comp2040.Assignments.Memory.Machine
{
    public interface ICpu
    {
        void Cycle ();
    }
}
