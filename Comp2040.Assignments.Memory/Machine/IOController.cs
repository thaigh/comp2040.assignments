﻿using System;
using System.Linq;
using System.Collections.Generic;
using Comp2040.Assignments.Memory.Models;

namespace Comp2040.Assignments.Memory.Machine
{
    public class IOController
    {
        public const int PageSwapTime = 7;

        private Cpu _cpu;
        private List<IoRequest> _ioRequests = new List<IoRequest> ();

        public EventHandler<PageReadyEventArgs> PageReady;

        public IOController(Cpu cpu)
        {
            _cpu = cpu;
            _cpu.OnCpuCycle += OnCpuCycle;
            _cpu.PageFault  += OnCpuPageFault;
        }

        private void OnCpuCycle (object sender, EventArgs e)
        {
            CheckForReadyPages ();
        }

        private void CheckForReadyPages()
        {
            var completedRequests = _ioRequests
                .Where (p => p.PageReadyTime <= _cpu.CurrentTime)
                .ToList();

            CompleteIoRequests (completedRequests);
        }

        private void CompleteIoRequests(IEnumerable<IoRequest> completedRequests)
        {
            foreach (IoRequest req in completedRequests) {
                Console.WriteLine ("++++ IO CONTROLLER: Page {0} is ready", req.PageToLoad);
                _ioRequests.Remove (req);

                if (PageReady != null)
                    PageReady (this, new PageReadyEventArgs (req.PageToLoad));
            }
        }

        void OnCpuPageFault (object sender, PageFaultEventHandler e)
        {
            // Issue an IO Request and wait for the results
            HandleIoRequest(e.PageToLoad);
        }

        private void HandleIoRequest(Page pageToLoad)
        {
            // Check to see if we already have a pending request for this page
            if (RequestExists(pageToLoad))
                return;

            // Create a new IO Request
            CreateNewIoRequest(pageToLoad);
        }

        public bool RequestExists(Page pageToLoad)
        {
            var page = _ioRequests.FirstOrDefault(r => r.PageToLoad.PageId == pageToLoad.PageId);
            return page != null;
        }

        private void CreateNewIoRequest(Page pageToLoad)
        {
            int readyTime = _cpu.CurrentTime + PageSwapTime;

            IoRequest req = new IoRequest
            {
                PageToLoad = pageToLoad,
                PageReadyTime = readyTime
            };

            _ioRequests.Add(req);

            Console.WriteLine("++++ IO CONTROLLER: Page {0} will be ready ready at time {1}", req.PageToLoad, readyTime);
        }
    }

    public class PageReadyEventArgs
    {
        public Page LoadedPage { get; private set; }

        public PageReadyEventArgs(Page loadedPage)
        {
            this.LoadedPage = loadedPage;
        }
    }

    public class IoRequest
    {
        public Page PageToLoad { get; set; }
        public int PageReadyTime { get; set; }
    }
}
