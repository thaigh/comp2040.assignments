﻿using System;
using System.Linq;
using Comp2040.Assignments.Memory.Models;
using Comp2040.Assignments.Memory.Algorithms.Replacement;
using System.Collections.Generic;

namespace Comp2040.Assignments.Memory.Machine
{
    public enum AllocationStrategy
    {
        /// <summary>
        /// Each process is allocated a set number of frames (split evenly when multiple processes are used).
        /// The process can only load the set number of pages into the Page Memory Block
        /// </summary>
        FixedFrame,

        /// <summary>
        /// Each process is allowed to allocate as many pages as it requires.
        /// </summary>
        Variable
    }

    public class PageMemoryBlock
    {
        public const int TotalFrameSpace = 20;

        private Page [] _pages = new Page [TotalFrameSpace];
        private IOController _pageio;
        public int FreePages { get; private set; }

        public AllocationStrategy AllocationStrategy { get; private set; }
        public int MaxFramesPerProcess { get; private set; }

        public bool IsEmpty { get { return FreePages == TotalFrameSpace; } }
        public bool IsFull { get { return FreePages == 0; } }

        IPageReplacementAlgorithm _replacementAlgorithm;

        public PageMemoryBlock(IOController io, IPageReplacementAlgorithm replacementAlgorithm) {
            _pageio = io;
            _pageio.PageReady += IOControllerPageReady;

            _replacementAlgorithm = replacementAlgorithm;

            FreePages = TotalFrameSpace;
        }


        #region AllocationStrategy

        public void UseFixedFrameAllocationStrategy(int numberOfProcesses) {
            AllocationStrategy = AllocationStrategy.FixedFrame;
            MaxFramesPerProcess = TotalFrameSpace / numberOfProcesses; // Note integer division could result in empty frame space?
        }

        public void UseVariableAllocationStrategy() {
            AllocationStrategy = AllocationStrategy.Variable;
            MaxFramesPerProcess = int.MaxValue; // or TotalFrameSpace
        }

        #endregion


        #region PageAccess

        public bool ContainsPage(Page p) { return ContainsPage(p.PageId); }
        public bool ContainsPage(int pageId) { return _pages.FirstOrDefault(p => p != null && p.PageId == pageId) != null; }

        public PageFrameMap GetPageById(int pageId) {
            for (int i = 0; i < TotalFrameSpace; i++) {
                if (!PageFrameIsEmpty(i))
                {
                    Page p = GetPageAtFrame(i);

                    if (p.PageId == pageId)
                        return new PageFrameMap {
                            FrameIndex = i,
                            Page = p
                        };
                }
            }

            // Not found.
            throw new Exception("Page with pageId not found");
        }

        public Page GetPageAtFrame(int frameIndex) { return _pages[frameIndex]; }
        public bool PageFrameIsEmpty(int frameIndex) { return GetPageAtFrame(frameIndex) == null; }

        public IEnumerable<PageFrameMap> GetPagesForProcess(SchedulingProcess proc)
        {
            for (int i = 0; i < _pages.Length; i++) {
                Page p = GetPageAtFrame(i);

                if (proc.ContainsPage(p))
                    yield return new PageFrameMap { FrameIndex = i, Page = p };
            }
        }

        public void RemoveProcessPages(SchedulingProcess proc)
        {
            for (int i = 0; i < TotalFrameSpace; i++) {
                if (!PageFrameIsEmpty(i) && proc.ContainsPage(GetPageAtFrame(i)))
                    UnloadPageFromFrame(i);
            }
        }

        #endregion


        #region PageLoading

        public void LoadProcessPages(SchedulingProcess proc)
        {
            int pagesToLoad = Math.Min(TotalFrameSpace, proc.PageCount);

            int pageCounter = 0;
            for (int i = 0; i < pagesToLoad; i++)
            {
                Page p = proc.GetPageAtIndex(proc.CurrentPageIndex + pageCounter);

                if (PageFrameIsEmpty(i))
                {
                    LoadPageIntoEmptyFrame(p, i);
                    pageCounter++;
                }
            }
        }

        public void LoadPageIntoEmptyFrame(Page p, int frameIndex)
        {
            PreconditionCheckForPageAtIndex(frameIndex, shouldExist: false);

            // Load the page into memory
            _pages[frameIndex] = p;
            p.LoadedInMemory = true;
            FreePages--;

            Console.WriteLine("Added Page {0} to memory frame {1}", p, frameIndex);
        }

        public void UnloadPageFromFrame(int frameIndex)
        {
            PreconditionCheckForPageAtIndex(frameIndex, shouldExist: true);

            Page removePage = _pages[frameIndex];

            // Unload page from memory
            _pages[frameIndex] = null;
            removePage.LoadedInMemory = false;
            FreePages++;

            Console.WriteLine("Removed Page {0} from memory frame {1}", removePage, frameIndex);
        }

        public void SwapPageAtFrame(Page newPage, int frameIndex)
        {
            if (!PageFrameIsEmpty(frameIndex))
                UnloadPageFromFrame(frameIndex);

            LoadPageIntoEmptyFrame(newPage, frameIndex);
        }

        #endregion


        public int NextEmptyFrame(int startingFromFrame = 0) {
            for (int i = startingFromFrame; i < TotalFrameSpace; i++)
            {
                if (PageFrameIsEmpty(i))
                    return i;
            }

            // Now loop back around, but only if we didn't start from 0
            if (startingFromFrame == 0) return -1; // Couldn't find any empty frames

            for (int i = 0; i < startingFromFrame; i++)
            {
                if (PageFrameIsEmpty(i))
                    return i;
            }

            return -1; // Still didn't find any empty frames
        }

        public PageFrameMap LeastRecentlyUsedPage()
        {
            int leastRecentlyUsedTime = int.MaxValue;
            int frameLeastRecentlyUsed = -1;

            for (int i = 0; i < TotalFrameSpace; i++)
            {
                Page p = _pages[i];
                if (!p.HasBeenUsed) continue;

                if (p.LastUsedTime < leastRecentlyUsedTime)
                {
                    leastRecentlyUsedTime = p.LastUsedTime;
                    frameLeastRecentlyUsed = i;
                }
            }

            return new PageFrameMap
            {
                FrameIndex = frameLeastRecentlyUsed,
                Page = frameLeastRecentlyUsed == -1 ? null : GetPageAtFrame(frameLeastRecentlyUsed)
            };
        }

        private void PreconditionCheckForPageAtIndex(int index, bool shouldExist)
        {
            if (shouldExist && _pages[index] == null)
                throw new Exception("There should be a page at this index but none was found");

            if (!shouldExist && _pages[index] != null)
                throw new Exception("There should not be a page at this index but a page was found");
        }

        private void IOControllerPageReady(object sender, PageReadyEventArgs args)
        {
            Page p = args.LoadedPage;

            int replacementFrame = _replacementAlgorithm.GetReplacementFrame(p.ParentProcess, this);
            if (replacementFrame >= 0 && replacementFrame < TotalFrameSpace)
                SwapPageAtFrame(args.LoadedPage, replacementFrame);
            else
                Console.WriteLine("Cannot load Page {0} into memory. Attempted to load into Frame Index {1}", p, replacementFrame);
        }
    }

    public class PageFrameMap
    {
        public int FrameIndex { get; set; }
        public Page Page { get; set; }
    }

}
