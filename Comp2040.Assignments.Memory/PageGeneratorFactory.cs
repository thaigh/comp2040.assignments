﻿using System;
using System.Collections.Generic;
using Comp2040.Assignments.Memory.Models;

namespace Comp2040.Assignments.Memory
{
    public class PageGeneratorFactory
    {
        private class PageLookupIndex
        {
            public int ProcessIndex { get; set; }
            public int PageIndex { get; set; }

            public PageLookupIndex(int procId, int pageId)
            {
                this.ProcessIndex = procId;
                this.PageIndex = pageId;
            }

            public override int GetHashCode ()
            {
                return HashHelper.GetHashCode (
                    ProcessIndex, PageIndex
                );
            }

            public override bool Equals (object obj)
            {
                if (!(obj is PageLookupIndex))
                    return false;

                PageLookupIndex o = (PageLookupIndex)obj;
                return this.ProcessIndex == o.ProcessIndex
                           && this.PageIndex == o.PageIndex;
            }

            public override string ToString()
            {
                return string.Format("[PageLookup: PID={0}, Page={1}]", ProcessIndex, PageIndex);
            }
        }

        private IDictionary<PageLookupIndex, Page> _generatedPages
           = new Dictionary<PageLookupIndex, Page> ();

        public Page GeneratePage(int processId, int pageNumber)
        {
            PageLookupIndex lookup = new PageLookupIndex (processId, pageNumber);

            if (_generatedPages.ContainsKey (lookup))
                return _generatedPages [lookup];

            Page p = new Page (pageNumber);
            _generatedPages [lookup] = p;

            return p;
        }

    }


}
