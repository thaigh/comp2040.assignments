﻿using System;
using System.Collections.Generic;
using System.Threading;
using Comp2040.Assignments.Threading.Models;
using System.Linq;

namespace Comp2040.Assignments.Threading
{
    public class Simulator : IThreadService
    {
        private int _northFarmers = 6;
        private int _southFarmers = 6;
        private bool _simulateWestDangerIsland = false;
        private bool _running = false;

        Island _islandNorth, _islandSouth, _islandWest;
        Bridge _primaryBridgeNS, _secondaryBridgeNW, _secondaryBridgeSW;

        private List<Farmer> _farmers = new List<Farmer> ();

        public Simulator (int northFarmers, int southFarmers, bool simulateWestDangerIsland)
        {
            _northFarmers = northFarmers;
            _southFarmers = southFarmers;
            _simulateWestDangerIsland = simulateWestDangerIsland;
        }

        public void Start(string[] args)
        {
            _running = true;
            InitialiseSimulation ();
            StartSimulation ();
        }

        public void Stop()
        {
            _running = false;
            KillFarmers ();
            WaitForThreadsToFinish ();
        }

        public bool IsRunning()
        {
            return _running;
        }

        private void InitialiseSimulation ()
        {
            CreateIslandsBridges ();
            CreateFarmers ();
        }

        private void CreateIslandsBridges()
        {
            _islandNorth = new Island ("North", safeIsland: true);
            _islandSouth = new Island ("South", safeIsland: true);
            _primaryBridgeNS = new Bridge (_islandNorth, _islandSouth);
            _primaryBridgeNS.ConnectIslands ();

            if (_simulateWestDangerIsland) {
                _islandWest = new Island ("West", safeIsland: false);
                _secondaryBridgeNW = new Bridge (_islandNorth, _islandWest);
                _secondaryBridgeSW = new Bridge (_islandSouth, _islandWest);

                _secondaryBridgeNW.ConnectIslands ();
                _secondaryBridgeSW.ConnectIslands ();
            }
        }

        private void CreateFarmers()
        {
            SpawnFarmersOnIsland (_islandNorth, _northFarmers);
            SpawnFarmersOnIsland (_islandSouth, _southFarmers);
        }

        private void SpawnFarmersOnIsland(Island homeIsland, int numberOfFarmersToSpawn)
        {
            for (int i = 0; i < numberOfFarmersToSpawn; i++) {
                Farmer f = new Farmer (homeIsland, i);
                _farmers.Add (f);
                homeIsland.Farmers.Add (f);
            }
        }

        private void StartSimulation ()
        {
            foreach (var f in _farmers) {
                f.Start (new string [] { });
            }
        }

        private void KillFarmers ()
        {
            foreach (var f in _farmers) {
                f.Stop ();
            }
        }

        private void WaitForThreadsToFinish()
        {
            while (_farmers.Where (f => !f.ThreadFinished).Any())
                Thread.Sleep (1000);
        }

        public void DisplayResults ()
        {
            Console.WriteLine ("{0,25} {1,10} {2,10}", "Farmer Name", "Crossings", "Percentage");
            int totalCrossings = _primaryBridgeNS.TotalFarmersCrossed;

            foreach (var f in _farmers) {
                Console.WriteLine ("{0,25} {1,10} {2,10}", f.Name, f.NumberOfCrossingsMade,
                                  f.NumberOfCrossingsMade / (float)totalCrossings);
            }
        }
    }
}
