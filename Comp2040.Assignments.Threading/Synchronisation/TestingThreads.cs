﻿using System;
using System.Threading;

namespace Comp2040.Assignments.Threading.Synchronisation
{
    internal class TestingThreads
    {
        static Semaphore sem = new Semaphore (10);
        static Random rand = new Random ();

        internal static void Run (string [] args)
        {
            for (int i = 0; i < 10; i++) {
                Thread t = new Thread (ThreadProc);
                t.Name = "Thread_" + i;
                t.Start ();
            }

            Console.ReadLine ();
        }


        private static void ThreadProc ()
        {
            string name = Thread.CurrentThread.Name;

            Console.WriteLine ("{0}: Aquiring semaphore...", name);
            sem.Acquire ();
            {
                int sleepTime = rand.Next (1, 4);
                Console.WriteLine ("{0}: Inside semaphore! Sleeping for {1} seconds", name, sleepTime);
                Thread.Sleep (sleepTime * 1000);
                Console.WriteLine ("{0}: Thread awake!", name, sleepTime);
            }
            sem.Release ();
            Console.WriteLine ("{0}: Released semaphore!", name);
        }
    }
}
