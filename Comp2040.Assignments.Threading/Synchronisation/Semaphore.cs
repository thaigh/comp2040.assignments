﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

namespace Comp2040.Assignments.Threading.Synchronisation
{
    public class Semaphore : ISemaphore
    {
        // Acts as java synchronise keyword
        private readonly object _semaphoreLock = new object ();

        // Not required, but useful to debug concurrency
        //private Queue<Guid> _waitingThreads = new Queue<Guid> ();

        private readonly int _minAvailablePermits = 0;
        private readonly int _maxAvailablePermits = 1;
        private int _currentAvailablePermits = 1;

        public Semaphore () : this (0, 1) { }
        public Semaphore(int maxPermits) : this(0, maxPermits) { }
        public Semaphore(int minPermits, int maxPermits)
        {
            lock(_semaphoreLock)
            {
                CheckSemaphorePermits (minPermits, maxPermits);

                _minAvailablePermits = minPermits;
                _maxAvailablePermits = maxPermits;
                _currentAvailablePermits = maxPermits;
            }
        }

        private void CheckSemaphorePermits (int minPermits, int maxPermits)
        {
            if (minPermits >= maxPermits)
                throw new ArgumentException ("Min number of permits must be less than max");
        }

        public int CurrentAvailablePermits ()
        {
            lock (_semaphoreLock) {
                return _currentAvailablePermits;
            }
        }

        public bool OutOfPermits ()
        {
            lock (_semaphoreLock) {
                return _currentAvailablePermits <= _minAvailablePermits;
            }
        }

        public void Acquire ()
        {
            Monitor.Enter (_semaphoreLock);
            try {
                Console.WriteLine ("{0}: ==Semaphore== There are {1} permit(s) available", Thread.CurrentThread.Name, _currentAvailablePermits);

                if (OutOfPermits ())
                    QueueWaitingThread ();

                // Either we had an available permit, or we waited and
                // we responded to a permit being made available
                Console.WriteLine ("{0}: ==Semaphore== Securing permit...", Thread.CurrentThread.Name);
                DecrementResourceCount ();
                Console.WriteLine ("{0}: ==Semaphore== There are now {1} permit(s) available", Thread.CurrentThread.Name, _currentAvailablePermits);
            } finally {
                Monitor.Exit (_semaphoreLock);
            }

        }

        private void DecrementResourceCount()
        {
            _currentAvailablePermits--;

            if (_currentAvailablePermits < _minAvailablePermits)
                throw new Exception ("Current permits less than min available permits");
        }

        private void QueueWaitingThread()
        {
            // Debug code to test threads with GUIDs
            //Guid guid = Guid.NewGuid ();
            //_waitingThreads.Enqueue (guid);
            //Console.WriteLine ("{0}: ==Semaphore== Waiting for semaphore to be available with guid {1}...", Thread.CurrentThread.Name, guid);

            Console.WriteLine ("{0}: ==Semaphore== Waiting for semaphore to be available...", Thread.CurrentThread.Name);

            // When you think about it, it is possible for a Thread (A) to signal this
            // waiting thread (B) when A releases it's semaphore, however a third thread (C)
            // or even thread A may re-acquire the semaphore before B has a chance to respond.
            // (i.e. thread B isn't given any processing time before C acquires the semaphore)
            // Without checking when B wakes up, B may think that the semaphore is available
            // for acquisition, when really, it is not. You have to loop here, until the
            // semaphore is truly available. Note that this can cause starvation, but
            // it's not something that can be controlled, simply due to the nature of concurrency.
            while (OutOfPermits ())
                Monitor.Wait (_semaphoreLock);
            
            Console.WriteLine ("{0}: ==Semaphore== I have been notified of an available permit!", Thread.CurrentThread.Name);
        }

        public void Release ()
        {
            Monitor.Enter (_semaphoreLock);
            try
            {
                Console.WriteLine ("{0}: ==Semaphore== Releasing semaphore...", Thread.CurrentThread.Name);
                IncrementResourceCount ();
                Console.WriteLine ("{0}: ==Semaphore== There are now {1} permit(s) available", Thread.CurrentThread.Name, _currentAvailablePermits);

                NotifyWaitingThreadsOfRelease ();
            } finally {
                Monitor.Exit (_semaphoreLock);
            }

        }

        private void IncrementResourceCount()
        {
            _currentAvailablePermits++;

            if (_currentAvailablePermits > _maxAvailablePermits)
                throw new Exception ("Current permits greater than max available permits");
        }

        private void NotifyWaitingThreadsOfRelease()
        {
            // Debug code for testing with GUIDs
            //var guid = _waitingThreads.Dequeue ();
            //Console.WriteLine ("{0}: Removing GUID {1} from waiting queue", Thread.CurrentThread.Name, guid);

            // Only pulse the "first" supposed thread waiting on the lock.
            // Without seeing the internal implementation of Pulse, it is hard to
            // know if the "first" waiting thread is notified, or any waiting thread is notified.
            //
            // You could implement a Queue<Thread> to track all waiting threads and only
            // permit the first thread in the queue to be notified
            Monitor.Pulse (_semaphoreLock);
        }

    }
}
