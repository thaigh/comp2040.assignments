﻿using System;
using System.Threading;

namespace Comp2040.Assignments.Threading.Synchronisation
{
    public interface ISemaphore
    {
        void Acquire ();
        void Release ();
        int CurrentAvailablePermits ();
        bool OutOfPermits ();
    }
}
