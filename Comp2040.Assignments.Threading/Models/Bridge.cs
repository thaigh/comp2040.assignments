﻿using System;
using System.Threading;
using Comp2040.Assignments.Threading.Synchronisation;

namespace Comp2040.Assignments.Threading.Models
{
    public class Bridge
    {
        private Synchronisation.Semaphore _sem = new Synchronisation.Semaphore ();

        public Island _islandA { get; private set; }
        public Island _islandB { get; private set; }

        public bool IsOccupied { get { return _sem.OutOfPermits (); } }

        public int TotalFarmersCrossed { get; private set; }

        public string Name { get { return string.Format ("{0}<->{1}", _islandA.Name, _islandB.Name); }}


        public Bridge (Island islandA, Island islandB)
        {
            _islandA = islandA;
            _islandB = islandB;
        }

        public void ConnectIslands()
        {
            _islandA.Bridges.Add (this);
            _islandB.Bridges.Add (this);
        }

        public void CrossBridge(int travelTime)
        {
            _sem.Acquire ();
            {
                Console.WriteLine ("{0}: Crossing the {1} bridge now...", Thread.CurrentThread.Name, Name);
                Thread.Sleep (travelTime);
                Console.WriteLine ("{0}: Finished crossing the {1} bridge!", Thread.CurrentThread.Name, Name);
                TotalFarmersCrossed++;
                Console.WriteLine ("Bridge {0} - Number of Crossings: {1} times",
                                   Name, TotalFarmersCrossed);
            }
            _sem.Release ();
        }

        public Island GetConnectingIsland(Island referenceIsland)
        {
            CheckIslandConnectsToBridge (referenceIsland);

            if (referenceIsland == _islandA) return _islandA;
            if (referenceIsland == _islandB) return _islandA;

            // Should never be thrown unless internal state is modified
            throw new Exception ("Uncaught logic exception. Island not found");
        }

        private void CheckIslandConnectsToBridge(Island referenceIsland)
        {
            if (!ConnectsToThisBridge (referenceIsland))
                throw new ArgumentException ("Island does not connect to this bridge", nameof (referenceIsland));
        }

        public bool ConnectsToThisBridge(Island referenceIsland)
        {
            if (referenceIsland == _islandA) return true;
            if (referenceIsland == _islandB) return true;

            // does not connect
            return false;
        }

    }
}
