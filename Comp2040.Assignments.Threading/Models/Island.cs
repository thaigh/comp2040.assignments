﻿using System;
using System.Collections.Generic;

namespace Comp2040.Assignments.Threading.Models
{
    public class Island
    {
        public string Name { get; private set; }
        public bool IsSafeIsland { get; private set; }

        public List<Bridge> Bridges { get; private set; }
        public List<Farmer> Farmers { get; private set; }

        public Island(string name, bool safeIsland)
        {
            Name = name;
            IsSafeIsland = safeIsland;
            Bridges = new List<Bridge> ();
            Farmers = new List<Farmer> ();
        }

    }
}
