﻿using System;
using System.Threading;
using System.Linq;

namespace Comp2040.Assignments.Threading.Models
{
    public class Farmer : IThreadService
    {
        public const int StepsToCrossBridgeWhenFull = 30;
        public const int StepsToCrossBridgeWhenEmpty = 15;
        public const int SimulationTimeScaleFactor = 10;

        public bool IsCarryingLoad { get; set; }
        public Island CurrentIsland { get; set; }
        public int Id { get; private set; }
        public int NumberOfCrossingsMade { get; private set; }

        public string Name { get { return string.Format ("Farmer_{0}_{1}", _homeIsland.Name, Id); }}

        private readonly Island _homeIsland;
        private bool _running;
        public bool ThreadFinished { get; private set; }
        
        public int StepsToCarryCurrentLoad {
            get {
                return IsCarryingLoad
                    ? StepsToCrossBridgeWhenFull
                    : StepsToCrossBridgeWhenEmpty;
            }
        }

        public Farmer(Island homeIsland, int id)
        {
            _homeIsland = homeIsland;
            CurrentIsland = _homeIsland;
            IsCarryingLoad = true;
            Id = id;
        }

        public void Start (string [] args)
        {
            _running = true;
            ThreadFinished = false;
            Thread t = new Thread (Run);
            t.Name = this.Name;
            t.Start ();
        }

        public void Stop ()
        {
            _running = false;
        }

        public bool IsRunning ()
        {
            return _running;
        }

        private void Run ()
        {
            while (_running)
            {
                // Get bridge
                Bridge bridgeToCross = GetBridgeToCross ();

                CheckBridgeIsNotNull (bridgeToCross);

                Console.WriteLine ("{0}: Waiting to cross bridge {1} with {2} load...",
                                   Thread.CurrentThread.Name,
                                   bridgeToCross.Name,
                                   IsCarryingLoad ? "full" : "empty"
                                  );

                // cross bridge with thread sleep
                CrossBridge (bridgeToCross);
                NumberOfCrossingsMade++;

                // change current island
                CurrentIsland = bridgeToCross.GetConnectingIsland (CurrentIsland);

                Console.WriteLine ("{0}: I am now in the {1} Island",
                                   Thread.CurrentThread.Name,
                                   CurrentIsland.Name);

                // Change load
                if (CurrentIsland.IsSafeIsland)
                    ChangeLoad ();
            }

            Console.WriteLine ("{0}: Finished processing.", Thread.CurrentThread.Name);
            ThreadFinished = true;
        }

        private void ChangeLoad()
        {
            bool isHome = CurrentIsland == _homeIsland;

            // change load
            if (IsCarryingLoad && !isHome)
                Console.WriteLine ("{0}: Changing load from {1} to {2}",
                               Thread.CurrentThread.Name,
                               IsCarryingLoad ? "full" : "empty",
                               isHome ? "full" : "empty"
                              );

            IsCarryingLoad = isHome;
        }

        private void CheckBridgeIsNotNull(Bridge bridgeToCross)
        {
            if (bridgeToCross == null)
                throw new Exception ("Bridge to cross is null");
        }

        private Bridge GetBridgeToCross()
        {
            return CurrentIsland.Bridges.FirstOrDefault ();
        }

        private void CrossBridge (Bridge bridgeToCross)
        {
            int travelTime = StepsToCarryCurrentLoad * SimulationTimeScaleFactor;
            bridgeToCross.CrossBridge (travelTime);
        }

    }
}
