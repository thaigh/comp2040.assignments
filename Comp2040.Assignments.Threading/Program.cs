﻿using System;
using System.Threading;
using Comp2040.Assignments.Threading.Models;

namespace Comp2040.Assignments.Threading
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Simulator sim = new Simulator (20, 20, false);
            sim.Start (new string[] { });
            Thread.Sleep (50000);
            sim.Stop ();
            sim.DisplayResults ();
        }

    }
}
