﻿using System;
namespace Comp2040.Assignments.Threading
{
    public interface IThreadService
    {
        void Start (string [] args);
        void Stop ();
        bool IsRunning ();
    }
}
